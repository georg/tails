[[!meta title="Help Desk's standard replies"]]
[[!meta robots="noindex"]]

[[!toc levels=2]]

Standard reply
==============

Dear Tails user,

We are a very small team working on very limited human resources, with
regards to that, we may not be able to reply to every request we
receive, or, we may not reply in a timely manner. In advance, we
apologize for that.

You should find many resources on our support page:

https://tails.boum.org/support

From there you should find links to your complete documentation, our FAQ
and our known issues page, that includes a page dedicated to graphic
card issues.

If you are getting the "Error starting GDM", please read the provided
link: https://tails.boum.org/gdm

It is very difficult to investigate such problems without having access
to the affected computer. Furthermore, even if we managed to investigate
the problem, unfortunately we would lack resources to solve it.

We need more information from you in 2 cases:

- **If this problem did not happen with an older version of Tails**:

  Please tell us which older version worked better.

- **If you find a way to workaround the problem**:

  Please tell us, and we will document it so that other affected users benefit from your findings.

Our best hope is that a future Linux driver update will solve the
problem.

For advanced Linux users: if you want to dig deeper on your own, you can
take it upstream after reproducing on a recent, non-Tails Linux system.

If your issue is related to the Tor Network itself, please consider
contacting the The Tor Project instead.

If your issue is related to software included in Tails, please consider
contacting their support channels.

Thanks for your understanding, and have a very nice day.

# Reply to hardware support reports for which we can't do anything

Hi,

Thank you for sharing your experience with us. We are sorry that Tails does not
work well on this computer.

It is very difficult to investigate such problems without having access to the
affected computer. Furthermore, even if we managed to investigate the problem,
unfortunately we would lack resources to solve it.

If you did not do it yet, please read our Support page:
https://tails.boum.org/support/

We need more information from you in 2 cases:

- **If this problem did not happen with an older version of Tails**:

  Please tell us which older version worked better.

- **If you find a way to workaround the problem**:

  Please tell us, and we will document it so that other affected users benefit from your findings.

Our best hope is that a future Linux driver update will solve the
problem.

For advanced Linux users: if you want to dig deeper on your own, you can take it
upstream after reproducing on a recent, non-Tails Linux system.

Thanks for your understanding :)

Known issue where we need feedback
==================================

Thanks for your report,

I think that your are experiencing this issue that has already been reported:

INSERT LINK TO ISSUE

Can you confirm that your issue is the same and provide requested feedback to GitLab issue?

Not enough information
======================

Thanks for taking the time to report this problem. Unfortunately we
can't help you for the moment because your description doesn't include
enough information.

Please read our bug reporting instructions:

https://tails.boum.org/doc/first_steps/bug_reporting

Your problem may be already known, please consult the known issues page:

https://tails.boum.org/support/known_issues/

We'd be grateful if you would then provide a more complete description of the problem:

INSERT SUPPORT QUESTION HERE

Apple hardware support (Mac)
============================

See ticket here for up to date details:
<https://gitlab.tails.boum.org/tails/tails/-/issues/17640>.

Reply for users who repeatedly treat us as search engines
=========================================================

> This reply is not advisable for first time reports, only for
> users that keep asking questions already covered by our
> documentation.

We are a very small team working on very limited human resources. A lot
of other people are asking for our help on our various support channels.
User support is an important task and is quite high in our priorities.
But all the time that we spend doing user support is time that we cannot
dedicate to improving Tails as such. So please, take this into
consideration when writing to us...

Not a Tails-dev question
========================

This is the mailing-list for Tails development.
For usage and support questions, please see instead:

	https://tails.boum.org/support/

Thanks!

No OpenPGP key on the keyservers
================================

We could not find an OpenPGP key corresponding to this email address on the keyservers.
Please send us a valid public OpenPGP key, or publish it on the keyservers.

Howto collaborate with Tails
============================

Hi,

Please see https://tails.boum.org/contribute/

Cheers,

Tails mirror is broken or scary SSL message
===========================================

Thanks for reporting this. We have transferred these details to the team that works on this at tails-mirrors@boum.org.

Templates for requesting feedback on GitLab issues
==================================================

## Asking a user to try to reproduce the issue with Debian-live

Could you try to start a live version of Debian on your computer and
see if you can reproduce the issue?

https://tails.boum.org/doc/first_steps/bug_reporting/#debian

With your help this issue could hopefully then be fixed in a future
version of Linux and thus Tails.

Thanks in advance!
